

import 'package:logging/logging.dart';
import 'package:pub_proxy_server/pub_proxy_server.dart';
import 'dart:io';

void main() {
  initLog();
  PermissionStore store = new PermissionStore();
  store.addPermission(new UserPermission("alexandre", "alexandre"));
  store.addPermission(new UserPermission("alexandre.maurel@gmail.com", "")); //necessary to publish
  store.addPermission(new UserPermission("heroku", "heroku"));
  //InitializeSSL();
  //start_pub_proxy_server(new PubFederatedRepo.localAndDartLangProxy(), port:443, permissionStore:null, isSecure:true);
  
  var repos = [new PubRepoImpl(new Store(new StoreSettings.fromDir("C:/Users/alex/Google Drive/pub_repo","C:/Users/alex/pub_repo_tmp"))), 
                 new DartLangRemoteRepo(new Store(new StoreSettings.fromRepoDirPrefix('cache')), new HttpClient())];
  PubFederatedRepo pubrepo = new PubFederatedRepo(repos);
  start_pub_proxy_server(pubrepo, port:8042, permissionStore:store, isSecure:false);
}
 
initLog(){
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name}:${rec.loggerName}: ${rec.time}: ${rec.message}');
    if (rec.error != null) print(rec.error);
    if (rec.stackTrace != null) print(rec.stackTrace);
  });
}

void InitializeSSL() {
    var testPkcertDatabase = Platform.script.resolve('pkcert').toFilePath();
    SecureSocket.initialize(database: testPkcertDatabase, password: 'a');
  }