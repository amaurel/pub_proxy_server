
https://developer.mozilla.org/en-US/docs/NSS_Sources_Building_Testing
https://developer.mozilla.org/fr/docs/NSS/tools/NSS_Tools_certutil#__Creating_a_New_Certificate_Database_

The database should be an NSS certificate database directory containing a cert9.db file, not a cert8.db file. This version of the database can be created using the NSS certutil tool with "sql:" in front of the absolute path of the database directory, or setting the environment variable [NSS_DEFAULT_DB_TYPE] to "sql".

mkdir pkcert
certutil -N -d sql:pkcert

password : a

certutil -L -d sql:pkcert

Creating a Certificate Request

certutil -R -s "CN=localhost" -o localhost.req -d sql:pkcert
certutil -S -s "CN=My Issuer" -n myissuer -x -t "C,C,C" -1 -2 -5 -m 1234 -f password.txt -d sql:pkcert
certutil -C -m 2345 -i localhost.req -o localhost.crt -c myissuer -d sql:pkcert
certutil -A -n localhost -t "p,p,p" -i localhost.crt -d sql:pkcert 